#ifndef OCTREE_HH_
#define OCTREE_HH_

#include <limits>
#include <vector>
#include <cmath>
#include <assert.h>
#include <stdlib.h>
#include <iostream>
#include <cstddef>
#include <list>
#include <algorithm>

#include <Eigen/Dense>

#include "point.hh"

namespace utils {

/* -------------------------------------------------------------------------- */
using UInt    = unsigned int;  ///< default unsigned integer integer type
using Int     = int;
using Real    = double;        ///< default floating point type
using VectorE = Eigen::Matrix<Real, 3, 1>;
using Vector  = Point<3>;

/* -----------------------------------------------------------------------*/
/* Data structure to contain neighbor data                                */
/*------------------------------------------------------------------------*/
struct NeighborData {
  Vector * point;
  Real     distance;
  UInt     index;

  NeighborData(Vector * a, Real & d, UInt & id) :
    point(a), distance(d), index(id) {
  }
};

/* -----------------------------------------------------------------------*/
/* Data structure to contain neighbor data                                */
/*------------------------------------------------------------------------*/
struct ByDistance { 
    bool operator()(NeighborData const &a, NeighborData const &b) { 
        return a.distance < b.distance;
    }
};

/* -----------------------------------------------------------------------*/
/* Global function to convert octants into bits and store in reverse      */
/*------------------------------------------------------------------------*/
inline Vector toBit(UInt number) {
  assert(number < 8);
  
  Vector bit;
  for (int i = 2; i >= 0; --i) {
    bit[i] = number%2;
    number /= 2;
  }

  return bit;
}

}


namespace tree {

enum class Type {
  branch,
  leaf
};

/* -------------------------------------------------------------------------- 
  Class node
 -------------------------------------------------------------------------- */
class Node {

  /* ------------------------------------------------------------------------ */
  /* Constructor/Destructors                                                  */
  /* ------------------------------------------------------------------------ */
public:
    
  Node() {};

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

public:

  virtual void add(utils::Vector * pt) {}; 

  virtual void add(utils::Vector * pt, utils::UInt & id) {};
  
  virtual void getChildIndexContainingPoint(const utils::Vector &,
					    utils::UInt &,
					    utils::Vector &,
					    utils::Vector &) {}


  /* ------------------------------------------------------------------------ */
  utils::Vector minimum() { return this->min; }

  /* ------------------------------------------------------------------------ */
  utils::Vector maximum() { return this->max; }

  /* ------------------------------------------------------------------------ */
  bool intersectsSphere(const utils::Vector & point, utils::Real & radius) {

    utils::Vector center = max + min;
    center *= 0.5;
    utils::Real radius_2 = center.distance(max);
    utils::Real distance = center.distance(point);
    utils::Real maximum  = radius_2 + radius;

    if (distance <  maximum) {
      return true;
    }

    return false;
  }

public:
  
  utils::Vector max;

  utils::Vector min;

};

/* -------------------------------------------------------------------------- */
/* Class Branch
/* -------------------------------------------------------------------------- */
class Branch : public Node {

public:

  Node * children[8];

  /* ------------------------------------------------------------------------ */
  /* Constructor/Destructors                                                  */
  /* ------------------------------------------------------------------------ */

public:
  
  Branch() : children() {}

  Branch(utils::Vector & min, utils::Vector & max) : children() {
    this->max = max;
    this->min = min;
  }

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
  
public:

  /* ------------------------------------------------------------------------ */
  Node * operator[](utils::UInt i) const {
    return children[i];
  }

  /* ------------------------------------------------------------------------ */
  void getChildIndexContainingPoint(const utils::Vector & point,
				    utils::UInt & index,
				    utils::Vector & mi,
				    utils::Vector & ma) {
    utils::Vector delta(max.x() - min.x(),
		   max.y() - min.y(),
		   max.z() - min.z());

    delta *= 0.5; // divided into half every time

    for (utils::UInt i = 0; i < 8; ++i) {

      utils::Vector bit = utils::toBit(i);
      utils::Vector bmin, bmax;
      
      for (utils::UInt n = 0; n < 3; ++n) {
	bmin[n] = min[n]  + bit[n]*delta[n];
	bmax[n] = bmin[n] + delta[n]; 
      }
      
      if (point.x() >= bmin.x() && point.x() <= bmax.x() &&
	  point.y() >= bmin.y() && point.y() <= bmax.y() &&
	  point.z() >= bmin.z() && point.z() <= bmax.z()) {
	index  = i;
	ma = bmax;
	mi = bmin;
	break;
      }
         
    }
  }

  /* ------------------------------------------------------------------------ */
  void getChildWithinRadius(const utils::Vector & point,
			    utils::Real & radius,
			    std::list<Node*> & candidates) {
        
      for (utils::UInt i = 0; i < 8; ++i) {
	if (this->children[i] == nullptr) continue;

	if (this->children[i]->intersectsSphere(point, radius))
	  candidates.push_back(this->children[i]);
      }    
  }

  friend class Octree;
};


/* -------------------------------------------------------------------------- */
/* Class Leaf
/* -------------------------------------------------------------------------- */
class Leaf : public Node {

  /* ------------------------------------------------------------------------ */
  /* Constructor/Destructors                                                  */
  /* ------------------------------------------------------------------------ */
public:

  Leaf() {}

  Leaf(utils::Vector & min, utils::Vector & max, Type decision) {
    this->min = min;
    this->max = max;
    this->type = decision; 
  }

public:

  /*  ----------------------------------------------------------------------- */
  inline utils::UInt depth() { return n_depth; }

  /* ------------------------------------------------------------------------ */
  inline bool isLeafNode() { return this->type == Type::leaf; }


  inline Type nodeType() { return this->type; } 
  
  /* ------------------------------------------------------------------------ */
  void add(utils::Vector * pt) {
    p.push_back(pt);
  }

  /* ------------------------------------------------------------------------ */
  void add(utils::Vector * pt, utils::UInt & id) {
    p.push_back(pt);
    ids.push_back(id);
  }
  
  /* ------------------------------------------------------------------------ */
  void print() {
    auto it  = p.begin();
    auto end = p.end();

    for (; it != end; ++it) {
      std::cout << **it << std::endl;
    }  
  }

  /* ------------------------------------------------------------------------ */
  utils::Vector * search(utils::Vector & query) {
    auto it  = p.begin();
    auto end = p.end();

    utils::Vector * find;
    utils::Real distance = 0.0;
    utils::Real minimum  = 0.0;
    for (; it != end; ++it) {
      distance = query.distance(**it);
      if (distance <= minimum) {
	minimum = distance;
	find = *it;
      }
    }

    return find;
  }

  /* ------------------------------------------------------------------------ */
  void search(utils::Vector & query, utils::Real & radius,
	      std::vector<utils::Vector*> & find,
	      std::vector<utils::Real>   & distances)  {

    auto it  = p.begin();
    auto end = p.end();

    utils::Real distance = 0.0;
    utils::Real minimum  = radius;
    for (; it != end; ++it) {
      distance = query.distance(**it);
      if (distance <= minimum) {
	find.push_back(*it);
	distances.push_back(distance);
      }
    }
  }

  /* ------------------------------------------------------------------------ */
  void search(utils::Vector & query,
	      utils::Real & radius,
	      std::vector<utils::NeighborData> & finds) {

    auto it  = p.begin();
    auto end = p.end();

    auto it2 = ids.begin();

    utils::Real distance = 0.0;
    utils::Real minimum  = radius;
    for (; it != end; ++it, ++it2) {
      distance = query.distance(**it);
      if (distance <= minimum) {
	finds.emplace_back(*it, distance, *it2);
      }
    }
  }
  
  /* ------------------------------------------------------------------------ */
  /* Members                                                                  */
  /* ------------------------------------------------------------------------ */
  utils::UInt n_depth;

  bool is_leaf;

  Type type;
  
  std::vector<utils::Vector* > p;

  std::vector<utils::UInt>   ids;
  
  friend class Octree;
};


/* -------------------------------------------------------------------------- */
/* Class Octree
/* -------------------------------------------------------------------------- */  
class Octree{  

  /* ------------------------------------------------------------------------ */
  /* Constructor/Destructors                                                  */
  /* ------------------------------------------------------------------------ */
public :

  Octree() = default;

  Octree(utils::UInt depth) : m_depth(depth), 
			      leaf_count(0),
			      branch_count(0),
			      data_count(0),
			      root(NULL) {
    assert(depth > 1);
  };

  ~Octree() = default;

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
  
public:

  /* ------------------------------------------------------------------------ */
  inline void limits(const utils::Vector & point, utils::Vector & minimum, utils::Vector & maximum) {
    for (utils::UInt i = 0; i < 3; i++) {
      minimum[i] = std::min(minimum[i], point[i]);
      maximum[i] = std::max(maximum[i], point[i]);
    }
  }

  /* ------------------------------------------ */
  inline void setLimits(std::vector<utils::Real> & dimensions) {
    for (utils::UInt i = 0; i < 3; i++) {
      minimum[i] = std::min(minimum[i], -dimensions[i]/2.0);
      maximum[i] = std::max(maximum[i],  dimensions[i]/2.0);
    }
  }

  
  /* ------------------------------------------------------------------------ */
  void build(std::vector<utils::Vector > & points)  {

    auto it  = points.begin();
    auto end = points.end();

    root = new Branch(minimum, maximum);

    it = points.begin();
    utils::UInt id = 0;
    for (; it != end; ++it, ++id) {
      this->insert(it, id);
    }

    assert( data_count == points.size() );
  }

  /* ------------------------------------------------------------------------ */
  Leaf * insert(std::vector<utils::Vector>::iterator it) {

    Node** branch = &root;
    
    utils::UInt index = 0;
    utils::UInt depth = m_depth;

    utils::Vector min = minimum;
    utils::Vector max = maximum;
   
    while (depth) {
      
      if (*branch == nullptr) {
	*branch = new Branch(min, max);
	nodes.push_back(branch);
	++branch_count;
      }
      
      (*branch)->getChildIndexContainingPoint(*it, index, min, max);
      branch = &reinterpret_cast<Branch*>(*branch)->children[index];
      --depth;
    }

    if (*branch == nullptr) {
      assert(depth == 0);

      *branch = new Leaf(min, max, Type::leaf);
      (*branch)->add(&(*it));
      nodes.push_back(branch);

      ++leaf_count;
      ++data_count;
    }
    else {
      assert(depth == 0);

      (*branch)->add(&(*it));
      ++data_count;
    }
    
    return reinterpret_cast<Leaf*>(*branch);
  }

    /* ------------------------------------------------------------------------ */
  Leaf * insert(std::vector<utils::Vector>::iterator it, utils::UInt & id) {

    Node** branch = &root;
    
    utils::UInt index = 0;
    utils::UInt depth = m_depth;

    utils::Vector min = minimum;
    utils::Vector max = maximum;
   
    while (depth) {
      
      if (*branch == nullptr) {
	*branch = new Branch(min, max);
	nodes.push_back(branch);
	++branch_count;
      }
      
      (*branch)->getChildIndexContainingPoint(*it, index, min, max);
      branch = &reinterpret_cast<Branch*>(*branch)->children[index];
      --depth;
    }

    if (*branch == nullptr) {
      assert(depth == 0);

      *branch = new Leaf(min, max, Type::leaf);
      (*branch)->add(&(*it), id);
      nodes.push_back(branch);

      ++leaf_count;
      ++data_count;
    }
    else {
      assert(depth == 0);

      (*branch)->add(&(*it), id);
      ++data_count;
    }
    
    return reinterpret_cast<Leaf*>(*branch);
  }
 
  /* ------------------------------------------------------------------------ */
  void search(utils::Vector & query) {

    Node**  branch = &root;
    Node**  parent;
    Leaf*   leaf;
    Branch* bbranch;
    
    utils::UInt index = 0;
    utils::UInt depth = m_depth;

    utils::Vector min = minimum;
    utils::Vector max = maximum;

    while (depth) {

      parent = branch;
      (*branch)->getChildIndexContainingPoint(query, index, min, max);
      branch = &reinterpret_cast<Branch*>(*branch)->children[index];
      --depth;
    }

    utils::Vector * find;
    leaf = reinterpret_cast<Leaf*>(*branch);
    if (leaf == nullptr) {
      bbranch = reinterpret_cast<Branch*>(*parent);
      
    }
    else {
      find = leaf->search(query);
    }
  }

  /* ------------------------------------------------------------------------ */
  void search(utils::Vector & query, utils::Real & radius) {

    Node** branch = &root;

    std::list<Node*>    candidates;
    std::vector<Node*>  leaves;

    candidates.push_back(*branch);

    // finding all possible leaves within given radius
    while (!candidates.empty()) {
      auto l = reinterpret_cast<Leaf*>(candidates.front());
      auto b = reinterpret_cast<Branch*>(candidates.front());

      if (l->nodeType() == Type::leaf && b->intersectsSphere(query, radius)) {
	leaves.push_back(l);
	candidates.pop_front();
	continue;
      }
      
      b->getChildWithinRadius(query, radius, candidates);
      candidates.pop_front();
    }

    // find k nearest neighbor within radius    
    auto it  = leaves.begin();
    auto end = leaves.end();

    Leaf * l;

    neighbors.clear();
    for (; it != end; ++it) {
      l = reinterpret_cast<Leaf*>(*it);
      l->search(query, radius, neighbors);
    }

    auto it2 = neighbors.begin();
    auto end2 = neighbors.end();

    std::sort(neighbors.begin(), neighbors.end(), utils::ByDistance());

  }

  /* ------------------------------------------------------------------------ */
  void getNeighbors(std::vector<utils::UInt> & indexes, std::vector<utils::Real> & distances) {

    auto it  = neighbors.begin();
    auto end = neighbors.end();

    for (; it != end; ++it) {
      distances.push_back((*it).distance);
      indexes.push_back((*it).index);
    }
  }

  /* ------------------------------------------ */
  void clear() {
    //root = NULL;
    data_count = 0;
    leaf_count = 0;
    nodes.clear();
    neighbors.clear();
  }
  
  /* ------------------------------------------------------------------------ */
  /*void visualize() {

    std::stringstream filename;
    filename << "octree" << ".vtu";

    auto writer       = vtkXMLUnstructuredGridWriterP::New();
    auto appendFilter = vtkAppendFilterP::New();
 
    utils::Vector min, max;

    auto it = nodes.begin();
    auto end = nodes.end();

    for (; it != end; ++it) {
    
      min =  (**it)->minimum();
      max =  (**it)->maximum();
      auto cubeSource = vtkCubeSourceP::New();
      cubeSource->SetBounds(min.x(), max.x(), min.y(), max.y(), min.z(), max.z());
      cubeSource->Update();
#if VTK_MAJOR_VERSION <= 5
      appendFilter->AddInput(cubeSource->GetOutput());
#else
      appendFilter->AddInputData(cubeSource->GetOutput());
#endif
      
    }

    appendFilter->Update();    
    const std::string & tmp = filename.str();
    const char* cstr = tmp.c_str();

    writer->SetFileName(cstr);
#if VTK_MAJOR_VERSION <= 5
  writer->SetInput(appendFilter->GetOutput());
#else
  writer->SetInputData(appendFilter->GetOutput());
#endif
  writer->Write();
  }*/
  
  /* ------------------------------------------------------------------------ */
  /* Members                                                                  */
  /* ------------------------------------------------------------------------ */
public:

  /// maximum depth of octree
  utils::UInt m_depth;

  /// root node 
  Node * root;

  /// total numbers of leaves in octree
  utils::UInt leaf_count;

  /// total numbers of intermediate nodes or branches
  utils::UInt branch_count;

  /// total number of NeighborData stored in leaves
  utils::UInt data_count;

  /// minimum of global bounding box
  utils::Vector minimum;

  /// maximum of global bounding box
  utils::Vector maximum;

  /// contains all branches and leaves
  std::vector<Node**> nodes;

  /// contains neighbors found
  std::vector<utils::NeighborData> neighbors;
};

}

#endif
