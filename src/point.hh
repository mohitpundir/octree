#ifndef __POINT_HH__
#define __POINT_HH__


#include <vector>
#include <cmath>
#include <iostream>

using UInt   = unsigned int;  ///< default unsigned integer integer type
using Int    = int;
using Real   = double;        ///< default floating point type


/// Class representing a point in space
template <UInt dim>
class Point {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

public:
  
  Point() {
    for (UInt i=0; i < dim; i++)
      coords[i] = 0.;
  }
  
  Point(Real point[dim]) {
    for (UInt i=0; i < dim; i++)
      coords[i] = point[i];
  }

  Point(UInt m_size, const Real * m_data) {
    if (m_data != NULL) {
      for (UInt i = 0; i < m_size; ++i) {
	coords[i] = m_data[i];
      }
    } 
  }

  Point(Real x, Real y, Real z)  {
    coords[0] = x;
    coords[1] = y;
    coords[2] = z;
  }


  ~Point() {  
  }
  
  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
  
public:

    inline Real x () const {
    return coords[0];
  }

  inline Real y () const {
    return coords[1];
  }

  inline Real z () const {
    return coords[2];
  }

  inline Real operator[] (UInt index) const {
    return coords[index];
  }

  inline Real& operator[] (UInt index) {
    return coords[index];
  }

  
  inline Point& operator*=(const Real & scalar) {
    for (UInt i =0 ; i < dim; i++) {
      coords[i] *= scalar;
    }
    return *this;
  }

  inline Point& operator/=(const Real & scalar) {
    for (UInt i =0 ; i < dim; i++) {
      coords[i] /= scalar;
    }
    return *this;
  }

  inline Point& operator+=(const Point & p) {
    for (UInt i =0 ; i < dim; i++) {
      coords[i] += p.coords[i];
    }
    return *this;
  }

  inline Point& operator-=(const Point & p) {
    for (UInt i =0 ; i < dim; i++) {
      coords[i] -= p.coords[i];
    }
    return *this;
  }

  inline Point& operator=(Real s) {
    for (UInt i = 0; i < dim; ++i) {
      coords[i] == s;
    }
    return *this;
  }

  inline Real distance(const Point & p) {
    Real distance = 0.0;
    for (UInt i = 0; i < dim; i++) {
      distance += pow(coords[i] - p.coords[i], 2);
    }

    return sqrt(distance);
  }

  inline void normalize() {
    Real norm = this->norm();
    for (UInt i=0; i < dim; i++) {
      coords[i] /= norm;
    }    
  }

  inline Real norm() {
    Real norm = 0.0;
    for (UInt i = 0; i < dim; ++i) {
      norm += pow(coords[i], 2);
    }
    return sqrt(norm);    
  }

  inline Real squaredNorm() {
    Real squared_norm = 0.0;
    for (UInt i = 0; i < dim; ++i) {
      squared_norm += pow(coords[i], 2);
    }
    return squared_norm;
  }

  inline Point normal(const Point & p) {

    Point<dim> dummy = p - *this;
    dummy.normalize();      
    return dummy;
  }

  inline void initself(std::istream & sstr) const {
    for (UInt i = 0; i < dim; i++)
      sstr >> coords[i];
  }
  
  inline void printself(std::ostream & stream) const {
    for (UInt i= 0; i < dim -1 ; i++)
      stream << coords[i] << " ";
    stream << coords[dim - 1];
  }

  inline Real size() {
    return dim;
  }

  inline Real dot(const Point & p) {
    Real product = 0.0;
    for (UInt i = 0; i < dim; ++i) {
      product += this->coords[i] * p.coords[i];
    }
    return product;
  }

  inline std::vector<size_t> getStrides(bool bytes) const {
    std::vector<size_t> ret(1);
    ret[0] = 1;

    if (bytes) {
      ret[0] *= sizeof(Real);
    }

    return ret;
  }

  inline std::vector<size_t> getShape() const {
    std::vector<size_t> ret(1);
    ret[0] = dim;

    return ret;
  }


  /* ------------------------------------------------------------------------ */
  /* Members                                                                  */
  /* ------------------------------------------------------------------------ */
  
private:

  Real coords[dim];
};


//! Addition between two points
template <UInt dim>
Point<dim> operator+(const Point<dim>& p, const Point<dim>& q) {
  Point<dim> r(p);
  return r += q;
}

//! Addition between two points
template <UInt dim>
Point<dim> operator+(Point<dim>& p, const Point<dim>& q) {
  Point<dim> r(p);
  return r += q;
}


//! Subtraction between two points
template <UInt dim>
Point<dim> operator-(const Point<dim>& p, const Point<dim>& q) {
  Point<dim> r(p);
  return r -= q;
}

template<UInt dim>
Point<dim> operator*(const Real & scalar, const Point<dim>& p) {
  Point<dim> r(p);
  return r *= scalar;
}

template<UInt dim>
Point<dim> operator/(const Point<dim>& p, const Real & scalar) {
  Point<dim> r(p);
  return r /= scalar;
}

template<UInt dim>
bool operator==(Point<dim>& p, Point<dim>& q) {
  if(p.size() != q.size()) return false;
  
  for (UInt i = 0; i < p.size(); ++i){
    if (p[i] == q[i])
      continue;
    else
      return false;
  }
  return true;
}


/* -------------------------------------------------------------------------- */
inline std::ostream &operator<<(std::ostream & sstr, Point<3> & _this)
{
  _this.printself(sstr);
  return sstr;
}



#endif //__POINT_HH__
