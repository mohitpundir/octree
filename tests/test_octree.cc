#include "octree.hh"
#include "point.hh"
#include <gtest/gtest.h>


TEST(OctreeTest, search) {
  Point<3> p1(0, 0, 0);
  Point<3> p2(0, 0, 1);
  Point<3> p3(1, 0, 0);
  Point<3> p4(0.1, 0, 0);
  Point<3> p5(10, 0, 0);

  std::vector<Point<3>> points;
  points.push_back(p1);
  points.push_back(p2);
  points.push_back(p3);
  points.push_back(p4);
  points.push_back(p5);


  
  tree::Octree tree(4);
  tree.build(points);

  Point<3> query(0.1, 0.1, 0);
  Real radius{1};
  tree.search(query, radius);

  for(auto neigh : tree.neighbors){
    ASSERT_TRUE(*(neigh.point) == points[neigh.index]);
  }
}
